package com.qa.opencart.pages;

import com.microsoft.playwright.Page;

public class LoginPage {

    private Page page;

    // String locators
    private String emailId = "#input-email";
    private String passwordId = "#input-password";
    private String loginButton = ".well form input[type='submit']";
    private String forgottenPas = ".well form a";
    private String logOutButton = "#column-right > div > a:nth-child(13)";

    public LoginPage(Page page) {
        this.page = page;
    }

    public String getLoginPageTitle() {
        return page.title();
    }

    public boolean isForgottenPwdLinkPresent() {
        return page.isVisible(forgottenPas);
    }

    public boolean doLogin(String user, String pass) {
        System.out.println("User: " + user);
        System.out.println("Pass: " + pass);
        page.fill(emailId, user);
        page.fill(passwordId, pass);
        page.click(loginButton);
        if (page.isVisible(logOutButton)) {
            System.out.println("User logged Ok");
            return true;
        }
        return false;

    }
}
