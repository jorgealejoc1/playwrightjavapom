package com.qa.opencart.pages;

import com.microsoft.playwright.Page;

public class HomePage {

    private Page page;

    // 1. String locator - Object Repository
    private String searchTextField = "input[name='search']";
    private String searchButton = "div #search button";
    private String resultPageHeader = "div#content h1";
    private String myAccountDropDown = "a[title='My Account']";
    //private String myAccountDropDown = "#top-links a[title='My Account']";
    private String loginButton = "a:text('Login')";

    // 2. Page constructor
    public HomePage(Page page) {
        this.page = page;
    }

    // 3. Page actions/methods
    public String getHomePageTitle() {
        String title = page.title();
        System.out.println("Title is: "+title);
        return title;
    }

    public String getHomePageURL() {
        String url = page.url();
        System.out.println("URL is: "+url);
        return url;
    }

    public String doSearch(String productName) {
        page.fill(searchTextField, productName);
        page.click(searchButton);
        String header = page.textContent(resultPageHeader);
        System.out.println("Search header: " + header);
        return header;
    }

    public LoginPage navigateToLoginPage() {
        page.click(myAccountDropDown);
        page.click(loginButton);
        return new LoginPage(page);
    }

}
