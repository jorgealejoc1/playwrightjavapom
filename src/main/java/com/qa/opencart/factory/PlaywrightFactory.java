package com.qa.opencart.factory;

import com.microsoft.playwright.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PlaywrightFactory {

    Playwright playwright;
    Browser browser;
    BrowserContext browserContext;
    Page page;
    Properties properties;

    private static ThreadLocal<Browser> threadLocalBrowser = new ThreadLocal<>();
    private static ThreadLocal<BrowserContext> threadLocalBrowserContext = new ThreadLocal<>();
    private static ThreadLocal<Page> threadLocalPage = new ThreadLocal<>();
    private static ThreadLocal<Playwright> threadLocalPlaywright = new ThreadLocal<>();

    public static Browser getBrowser() {
        return threadLocalBrowser.get();
    }

    public static BrowserContext getBrowserContext() {
        return threadLocalBrowserContext.get();
    }

    public static Page getPage() {
        return threadLocalPage.get();
    }

    public static Playwright getPlaywright() {
        return threadLocalPlaywright.get();
    }

    public Page initBrowser(Properties properties) {
        String browserName = properties.getProperty("browser").trim();
        System.out.println("Browser name is: "+browserName);

        playwright = Playwright.create();
        threadLocalPlaywright.set(Playwright.create());

        // Launch or connect to Chromium depends on browser name
        switch (browserName) {
            case "chromium":
                //browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false));
                threadLocalBrowser.set(playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false)));
                break;

            case "firefox":
                //browser = playwright.firefox().launch(new BrowserType.LaunchOptions().setHeadless(false));
                threadLocalBrowser.set(playwright.firefox().launch(new BrowserType.LaunchOptions().setHeadless(false)));
                break;

            case "safari":
                //browser = playwright.webkit().launch(new BrowserType.LaunchOptions().setHeadless(false));
                threadLocalBrowser.set(playwright.webkit().launch(new BrowserType.LaunchOptions().setHeadless(false)));
                break;

            case "chrome":
                //browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setChannel("chrome").setHeadless(false));
                threadLocalBrowser.set(playwright.chromium().launch(new BrowserType.LaunchOptions().setChannel("chrome").setHeadless(false)));
                break;

            default:
                System.out.println("Please pass a valid browser name!");
                break;
        }

        /*browserContext = browser.newContext();
        page = browserContext.newPage();
        page.navigate(properties.getProperty("url"));*/

        threadLocalBrowserContext.set(getBrowser().newContext());
        threadLocalPage.set(getBrowserContext().newPage());
        getPage().navigate(properties.getProperty("url").trim());

        return getPage();

    }

    // This method is used to initialize the properties from config file
    public Properties init_prop() {
        try {
            FileInputStream fileInputStream = new FileInputStream("./src/test/resources/config/config.properties");
            properties = new Properties();
            try {
                properties.load(fileInputStream);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        return properties;
    }

}
