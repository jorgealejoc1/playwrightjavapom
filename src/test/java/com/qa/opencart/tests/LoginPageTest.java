package com.qa.opencart.tests;

import com.qa.opencart.base.BaseTest;
import com.qa.opencart.constants.AppConstants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

public class LoginPageTest extends BaseTest {

    @Test
    @Order(1)
    public void LoginPageNavigationTest() {
        loginPage = homePage.navigateToLoginPage();
        String loginPageTitle = loginPage.getLoginPageTitle();
        System.out.println("Login page title: " + loginPageTitle);
        Assertions.assertEquals(AppConstants.LOGIN_PAGE_TITLE, loginPageTitle);

        //Assertions.assertTrue (loginPage.isForgottenPwdLinkPresent());
        forgotPwdButtonExistTest();

        //Assertions.assertTrue(loginPage.doLogin(properties.getProperty("username").trim(), properties.getProperty("password").trim()));
        appLoginTest();

    }

    //@Test
    @Order(2)
    public void forgotPwdButtonExistTest() {
        Assertions.assertTrue (loginPage.isForgottenPwdLinkPresent());
    }

    //@Test
    @Order(3)
    public void appLoginTest() {
        Assertions.assertTrue(loginPage.doLogin(properties.getProperty("username").trim(), properties.getProperty("password").trim()));
    }

}
