package com.qa.opencart.tests;

import com.qa.opencart.base.BaseTest;
import com.qa.opencart.constants.AppConstants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class HomePageTest extends BaseTest {

    @Tag("regression")
    @Test
    public void homePageTitleTest() {
        String currentTitle = homePage.getHomePageTitle();
        Assertions.assertEquals(AppConstants.HOME_PAGE_TITLE, currentTitle);
    }

    @Tag("regression")
    @Test
    public void homePageURLTest() {
        String currentURL = homePage.getHomePageURL();
        Assertions.assertEquals(properties.getProperty("url"), currentURL);
    }

    @Tag("sanity")
    @ParameterizedTest
    @ValueSource(strings = {"Macbook", "iMac", "Samsung"})
    public void SearchTest(String productName) {
        String actualResult = homePage.doSearch(productName);
        Assertions.assertEquals("Search - " + productName, actualResult);
    }

}
