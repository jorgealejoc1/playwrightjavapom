package com.qa.opencart.base;

import com.microsoft.playwright.Page;
import com.qa.opencart.factory.PlaywrightFactory;
import com.qa.opencart.pages.HomePage;
import com.qa.opencart.pages.LoginPage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.util.Properties;

public class BaseTest {

    PlaywrightFactory playwrightFactory;
    Page page;
    protected Properties properties;
    protected HomePage homePage;
    protected LoginPage loginPage;

    @BeforeEach
    public void setUp() {
        playwrightFactory = new PlaywrightFactory();
        properties = playwrightFactory.init_prop();
        page = playwrightFactory.initBrowser(properties);
        homePage = new HomePage(page);
    }

    @AfterEach
    public void tearDown() {
        page.context().browser().close();
    }

}
